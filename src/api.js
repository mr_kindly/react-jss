import axios from 'axios';

const productUrl = '/api/v1/products/';
const categoryUrl = '/api/v1/categories/';
const apiHost = 'https://remp-api.herokuapp.com';

export async function getProduct(id) {
  return await axios.get(`${apiHost}${productUrl}${id}`).then(res => res.data);
}

export async function getCategoryProducts(id) {
  const url = `${apiHost}${categoryUrl}${id}/products`;
  console.log(url);
  return axios.get(`${apiHost}${categoryUrl}${id}/products`).then(res => res.data).catch(err => console.error(err));
}

export async function getCategory(id) {
  return await axios.get(`${apiHost}${categoryUrl}${id}`).then(res => res.data);
}

export async function getCategories() {
  return await axios.get(`${apiHost}${categoryUrl}`).then(res => res.data);
}
