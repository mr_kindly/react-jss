import React from 'react';
import classnames from 'classnames';
import './Price.scss';


const Price = ({ price }) => {
  const { value, discount, currency } = price;
  const newValue = value * discount / 100;
  const priceStyle = classnames({
    price: true,
    'price--crossed': !!discount,
  });

  return (
        <h4>
            <span className={priceStyle}>
                {value}{currency}
            </span>
            {!!discount && <span className="price--discont"> {newValue}{currency} </span>}
        </h4>);
};

export default Price;
