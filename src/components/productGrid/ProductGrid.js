import React from 'react';
import ProductTile from './../productTile';


const ProductGrid = ({ products }) => (<div className="row">
        {
            products.map(product => <div className="col-xs-12 col-md-4" key={product.id}>
                    <ProductTile product={product} withShowMore={true} />
                </div>)
        }
    </div>);

export default ProductGrid;
