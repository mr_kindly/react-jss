import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchCategoryProducts } from './../../store/actions/product';
import ProductGrid from '../../components/productGrid';


class CategoryPage extends React.PureComponent {
  componentDidMount() {
    this.getCategoryData();
  }

  componentWillReceiveProps(nextProps) {
    const { match: { params: nextParams } } = nextProps;
    const { match: { params: currentParams } } = this.props;
    if (nextParams.id !== currentParams.id) {
      this.getCategoryData(nextProps);
    }
  }

  getCategoryData(props = this.props) {
    const { match: { params } } = props;
    const { id } = params;

    if (id) {
      this.props.fetchCategoryProducts(id);
    }
  }
  render() {
    const { products } = this.props;
    return !products ? <h2>Product's list is empty</h2> : <ProductGrid products={products} />;
  }
}

export default connect(
  ({ category }, { match: { params } }) => {
    const { products } = category;

    return {
      products,
    };
  },
  {
    fetchCategoryProducts,
  },
)(CategoryPage);
