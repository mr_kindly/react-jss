import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchProduct } from './../../store/actions/product';
import ProductTile from './../../components/productTile';


class ProductPage extends Component {
  componentDidMount() {
    this.getProduct();
  }

  componentWillReceiveProps(nextProps) {
    const { match: { params: nextParams } } = nextProps;
    const { match: { params: currentParams } } = this.props;
    if (nextParams.id !== currentParams.id) {
      this.getProduct(nextProps);
    }
  }

  getProduct(props = this.props) {
    const { match: { params } } = props;
    const { id } = params;

    if (id) {
      this.props.fetchProduct(id);
    }
  }

  render() {
    const { product } = this.props;

    return !product ? <h3>Product doesn't find</h3> : (
            <div className="row">
                <div className="col-xs-12 col-md-8 col-md-offset-2">
                    <div className="row">
                        <div className="col-xs-12">
                            <Link to={`/category/${product.categoryId}`} className="btn btn-primary">Back to category</Link>
                        </div>
                        <div className="col-xs-12">
                            <ProductTile product={product} withShowMore={false} />
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}

export default connect(
  ({ product }, { match: { params } }) => {
    const { data } = product;

    return {
      product: data,
    };
  },
  {
    fetchProduct,
  },
)(ProductPage);
